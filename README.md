# Property Based Testing with Proper, Erlang and Elixir

Code and exercises from *"Property Based Testing with
Proper, Erlang and Elixir"* by Fred Hebert.

- https://propertesting.com/
- https://pragprog.com/titles/fhproper/

ISBN: 9781680506211

