defmodule Pbt do
  @moduledoc """
  Property Based Testing, with PropEr, Erlang and Elixir
  """

  def biggest([head | tail]) when is_integer(head) do
    biggest(tail, head)
  end

  defp biggest([], max), do: max

  defp biggest([head | tail], max) do
    cond do
      head > max  -> biggest(tail, head)
      head <= max -> biggest(tail, max)
    end
  end


  def increments([head | tail]), do: increments(head, tail)

  defp increments(_, []),
    do: true
  defp increments(n, [head | tail]) when head == n + 1,
    do: increments(head, tail)
  defp increments(_, _),
    do: false


  def encode(x), do: :erlang.term_to_binary(x)
  def decode(x), do: :erlang.binary_to_term(x)

  @space 0x20

  def word_count(s) when is_list(s) do
    word_count(s, @space, 0)
  end

  defp word_count([], _prev, count), do: count
  defp word_count([@space | tail], _prev, count) do
    word_count(tail, @space, count)
  end
  defp word_count([head | tail], @space, count) do
    word_count(tail, head, count + 1)
  end
  defp word_count([head | tail], _prev, count) do
    word_count(tail, head, count)
  end
end
