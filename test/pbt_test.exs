defmodule PbtTest do
  use ExUnit.Case
  use PropCheck

  # Properties
  #
  # Form:
  #   property "some description" do
  #     forall instance_of_type <- type_generator do
  #       property_expression
  #     end
  #   end
  property "finds biggest element" do
    forall x <- non_empty(list(integer())) do
      Pbt.biggest(x) == model_biggest(x)
    end
  end

  property "Range(start..end) produces the range [start..end]" do
    forall {start, count} <- {integer(), non_neg_integer()} do
      list = Enum.to_list(start .. (start + count))
      count + 1 == length(list) and Pbt.increments(list)
    end
  end

  property "List.last/1 returns the last element in a non-empty list" do
    forall {list, number} <- {list(number()), number()} do
      List.last(list ++ [number]) === number
    end
  end

  property "Enum.sort/1 sorts into ascending order" do
    forall list <- list(term()) do
      is_ordered(Enum.sort(list))
    end
  end

  property "Enum.sort/1 output is the same length as the input" do
    forall list <- list(number()) do
      length(Enum.sort(list)) == length(list)
    end
  end

  property "Enum.sort/1 doesn't add elements" do
    forall list <- list(number()) do
      sorted = Enum.sort(list)
      Enum.all?(sorted, fn element -> element in list end)
    end
  end

  property "Enum.sort/1 doesn't remove elements" do
    forall list <- list(number()) do
      sorted = Enum.sort(list)
      Enum.all?(list, fn element -> element in sorted end)
    end
  end


  # :lists.keysort/2 - sorts a list of tuples by their nth element.
  # property "List.keysort/2 sorts by the nth element" do
  # end
  
  # [{[], [0, 0]}]
  property "set union" do
    forall {list_a, list_b} <- {list(number()), list(number())} do
      set_a = MapSet.new(list_a)
      set_b = MapSet.new(list_b)
      model_union = Enum.sort(list_a ++ list_b) |> Enum.uniq

      res =
        MapSet.union(set_a, set_b)
        |> MapSet.to_list()
        |> Enum.sort()

      res == model_union
    end
  end

  ## Symmetric properties
  property "symmetric encoding/decoding" do
    forall data <- list({atom(), any()}) do
      encoded = Pbt.encode(data)
      is_binary(encoded) and Pbt.decode(encoded) == data
    end
  end


  ## 1.3 - Question 5
  property "word_count() counts words in char-lists separated by spaces" do
    word = resize(50, list(char()))
    forall words <- list(word) do
      wc = :lists.join(' ', words)
           |> :string.strip
           |> :string.tokens(' ')
           |> length
      Pbt.word_count(words) == wc
    end
  end


  # Helpers
  defp model_biggest(list) do
    List.last(Enum.sort(list))
  end

  defp is_ordered([a, b | tail]) do
    a <= b and is_ordered([b | tail])
  end
  defp is_ordered([_]), do: true
  defp is_ordered([]), do: true

  # Generators
  def my_type() do
    term()
  end
end
